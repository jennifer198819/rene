<!DOCTYPE html>
<!--[if IE 9 ]><html class="ie9"><![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bienvenidos a TuPont.com</title>

        <!-- Vendor CSS -->
        <link href="vendors/bower_components/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet">
        <link href="vendors/bower_components/animate.css/animate.min.css" rel="stylesheet">
        <link href="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css" rel="stylesheet">
        <link href="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet">        
            
        <!-- CSS -->
        <link href="css/app.min.1.css" rel="stylesheet">
        <link href="css/app.min.2.css" rel="stylesheet">
        <link href="css/estilos.css" rel="stylesheet">
    </head>
    <body>
        <header id="header" class="clearfix" data-current-skin="orange">
            <ul class="header-inner">
                <li id="menu-trigger" data-trigger="#sidebar">
                    <div class="line-wrap">
                        <div class="line top"></div>
                        <div class="line center"></div>
                        <div class="line bottom"></div>
                    </div>
                </li>

                <li class="logo hidden-xs">
                    <a href="index.html">Portal</a>
                </li>

                <li class="pull-right">
                    <ul class="top-menu">
                        <li id="top-search">
                            <a href=""><i class="tm-icon zmdi zmdi-search"></i></a>
                        </li>
                        <li class="dropdown">
                            <a data-toggle="dropdown" href=""><i class="tm-icon zmdi zmdi-more-vert"></i></a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Top Search Content -->
            <div id="top-search-wrap">
                <div class="tsw-inner">
                    <i id="top-search-close" class="zmdi zmdi-arrow-left"></i>
                    <input type="text">
                </div>
            </div>
        </header>
        
        <section id="main" data-layout="layout-1">
            <div class="carrusel">
                 <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                            </ol>
                          <div class="logo-slider">
                              <div class="imagen">
                                  <img src="img/logo.png" width="100%">
                              </div>
                          </div>
                            <!-- Wrapper for slides -->
                            <div class="carousel-inner" role="listbox">
                                <div class="item active">
                                    <img src="img/machu.jpg" alt="">
                                    <!-- <div class="carousel-caption" style="margin-top: -100px;">
                                        <h3>First Slide Label</h3>
                                        <p>Some sample text goes here...</p>
                                    </div> -->
                                </div>
                                <div class="item">
                                    <img src="img/cusco.jpg" alt="">
                                </div>
                                <div class="item">
                                    <img src="img/machu.jpg" alt="">
                                </div>
                            </div>
                          
                            <!-- Controls -->
                            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                                <span class="zmdi zmdi-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                                <span class="zmdi zmdi-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>        
            </div>   
            <section id="content">
                <div class="container">              
                    <div class="dash-widgets">
                        <div class="row margenes">
                            <a  href="busqueda.php" role="button" data-slide="next">
                            <div class="col-xs-6 col-sm-3 col-md-3  margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/1.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido menos-margen" style="text-decoration:none">Encuentra lo mejor de la gastronomia de Cusco </span>
                                    </div>
                                </div>
                            </div>
                            </a>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/4.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Hospedaje</h4>
                                         <span class="contenido">Hoteles, Hospedajes, Hostales y Habitacion</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/5.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Ropa</h4>
                                         <span class="contenido">Vestidos, regalos, zapatos</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/7.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Salud</h4>
                                         <span class="contenido">Encuentre doctores especialistas, medicamentos y servicios</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3  margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/1.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido menos-margen">Pellentesque lacinia sagittis libero. Praesent </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/5.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido">Pellentesque lacinia sagittis libero. Praesent</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/4.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido">Pellentesque lacinia sagittis libero. Praesent </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/7.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido">Pellentesque lacinia sagittis libero. Praesent</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3  margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/1.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido menos-margen">Pellentesque lacinia sagittis libero. Praesent </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/4.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido">Pellentesque lacinia sagittis libero. Praesent </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/5.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido">Pellentesque lacinia sagittis libero. Praesent</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-3 col-md-3 margenes tercero">
                                <div class="card blog-post">
                                    <div class="bp-header imagen_bp">
                                        <img src="img/7.jpg" alt="">
                                    </div>
                                    <div class="p-20 pequenio">
                                        <h4 class="color-titulos">Comida</h4>
                                         <span class="contenido">Pellentesque lacinia sagittis libero. Praesent</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
        
        <footer id="footer">
            Copyright &copy; TuPoint
            
            <ul class="f-menu">
                <li><a href="">Inicio</a></li>
                <li><a href="">Nosotros</a></li>
                <li><a href="">Servicios</a></li>
                <li><a href="">Soporte</a></li>
                <li><a href="">Contacto</a></li>
            </ul>
        </footer>

        <!-- Page Loader -->
        <div class="page-loader">
            <div class="preloader pls-blue">
                <svg class="pl-circular" viewBox="25 25 50 50">
                    <circle class="plc-path" cx="50" cy="50" r="20" />
                </svg>

                <p>Please wait...</p>
            </div>
        </div>

        
        <!-- Javascript Libraries -->
        <script src="vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        
        <script src="vendors/bower_components/flot/jquery.flot.js"></script>
        <script src="vendors/bower_components/flot/jquery.flot.resize.js"></script>
        <script src="vendors/bower_components/flot.curvedlines/curvedLines.js"></script>
        <script src="vendors/sparklines/jquery.sparkline.min.js"></script>
        <script src="vendors/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        
        <script src="vendors/bower_components/moment/min/moment.min.js"></script>
        <script src="vendors/bower_components/fullcalendar/dist/fullcalendar.min.js "></script>
        <script src="vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js"></script>
        <script src="vendors/bower_components/Waves/dist/waves.min.js"></script>
        <script src="vendors/bootstrap-growl/bootstrap-growl.min.js"></script>
        <script src="vendors/bower_components/bootstrap-sweetalert/lib/sweet-alert.min.js"></script>
        <script src="vendors/bower_components/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        
        <!-- Placeholder for IE9 -->
        <!--[if IE 9 ]>
            <script src="vendors/bower_components/jquery-placeholder/jquery.placeholder.min.js"></script>
        <![endif]-->
        
        <script src="js/flot-charts/curved-line-chart.js"></script>
        <script src="js/flot-charts/line-chart.js"></script>
        <script src="js/charts.js"></script>
        
        <script src="js/charts.js"></script>
        <script src="js/functions.js"></script>
        <script src="js/demo.js"></script>

        <script type="text/javascript" src="js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="js/jssor.slider.mini.js"></script>  

        
    </body>
  </html>